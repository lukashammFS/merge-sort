def merge_sort(array):
    if len(array) <= 1:
        return array

    middle = (len(array)//2)
    left = merge_sort(array[:middle])
    right = merge_sort(array[middle:])

    return reduce(left, right)
    
def reduce(left, right):
    sorted_list = []
    left_index=0
    right_index=0
    while True:

        if left[left_index] > right[right_index]:
            sorted_list.append(right[right_index])
            right_index += 1
        else:
            sorted_list.append(left[left_index])
            left_index += 1    
        if left_index == len(left):
            sorted_list.extend(right[right_index:])
            break
        if right_index == len(right):
            sorted_list.extend(left[left_index:])
            break
    return sorted_list