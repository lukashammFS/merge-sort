#import packages
import time
import random
import pandas as pd
import matplotlib.pyplot as plt
import math
import Merge_Sort

#define lists and arrays
def average_case(elements):
    avg = (list(range(1, elements+1)))
    random.shuffle(avg)
    return avg

def best_case(elements):
    return list(range(1, elements+1))

def worst_case(elements):
    return list(reversed(list(range(1, elements+1))))

test_element_array = [1000, 2000, 4000, 8000, 16000, 32000]

def insertion_time(array):
    time1 = time.time()
    Merge_Sort.merge_sort(array)
    time2 = time.time()
    return time2 - time1

#create arrays with times
best_case_times = []
for i in test_element_array:
    times = insertion_time(best_case(i))
    best_case_times.append(times)

average_case_times = []
for i in test_element_array:
    times = insertion_time(average_case(i))
    average_case_times.append(times)

worst_case_times = []
for i in test_element_array:
    times = insertion_time(worst_case(i))
    worst_case_times.append(times)

# "Insertion Sort time complexity by number of elements and case (times in seconds)"
df = pd.DataFrame({"Best Case": best_case_times, "Average Case": average_case_times, #"Worst Case": worst_case_times
                  },
                  index=test_element_array)
df.index.name = 'Number of Elements'

print(df)

#plot graph
plt.plot(df)
plt.xlabel('n') 
plt.ylabel('seconds') 
plt.title('Relationship between length of array and time complexity')
plt.legend(['Best Case','Average Case','Worst Case'])